﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace XrayWall
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool isServerRunning = false;
        public Thread t1;
        SQLiteConnection sqlite_conn;


        public MainWindow()
        {
            InitializeComponent();
            if(Database.isProtectionHigh())
            {
                HighScan.IsChecked = true;
            }
            Process[] pname = Process.GetProcessesByName("mitmproxy");
            if (pname.Length > 0)
            {
                isServerRunning = true;
                StartProxy.Content = "Stop Proxy";
            }
            sqlite_conn = Database.CreateConnection();
            ThreadStart ts1 = new ThreadStart(CheckThreats);
            t1 = new Thread(ts1);
            t1.Start();
        }
        
        void OnStartProxy(object sender, RoutedEventArgs e)
        {
            Process[] pname = Process.GetProcessesByName("mitmproxy");
            if (pname.Length > 0)
            {
                //close our proxy server and the cmd.
                ParentProcessUtilities.GetParentProcess(pname[0].Handle).Kill();
                pname[0].Kill();
                StartProxy.Content = "Start Proxy";
            }
            else
            {
                /*string strCmdText;
                //open little window with textbox and 2 buttons (run & cancel) so the user can enter the path of his mitmproxy server.
                //when the run btn clicked make sure first that the entered path is really exist in the user pc (check in google how to get response from cmd).
                //and also check that the mitmproxy.exe file and the MITM.py file really there (otherwise pop error window for the user with explainations).
                strCmdText = "/C cd C:/Users/user/mitmp & mitmproxy -s MITM.py"; //just for test the location of my project.
                System.Diagnostics.Process.Start("CMD.exe", strCmdText);
                this.isServerRunning = true;
                StartProxy.Content = "Stop Proxy";*/

                //optional, do so the proxy scans and everything is hidden, but that means Close button needed and it will close the proccess of the python run.
                //do it as Bonus if there is time and the project is completed.
                
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "CMD.exe";
                startInfo.Arguments = "/C cd C:/Users/user/mitmp & mitmproxy -s MITM.py";
                process.StartInfo = startInfo;
                process.Start();
                this.isServerRunning = true;
                StartProxy.Content = "Stop Proxy";

            }
        }
        void OnClickLogs(object sender, RoutedEventArgs e)
        {
            //move to the db window which show all the Threats History, and also buttons that change the db (add to blocked, white or remove from them).
            Logs win2 = new Logs();
            win2.Top = this.Top;
            win2.Left = this.Left;
            win2.Show();
            this.Close();
        }

        void OnCheckLow(object sender, RoutedEventArgs e)
        {
            if(HighScan != null) //low scan is created before highScan so it may be null.
            {
                if(HighScan.IsChecked == true)
                {
                    HighScan.IsChecked = false;
                    //change the scripts database to low scripts.
                    Database.setProtectionLow();
                }
            }
        }

        void OnUnCheckLow(object sender, RoutedEventArgs e)
        {
            if (HighScan != null) //low scan is created before highScan so it may be null.
            {
                if (HighScan.IsChecked == false)
                {
                    LowScan.IsChecked = true;
                }
            }
        }

        void OnCheckHigh(object sender, RoutedEventArgs e)
        {
            if(LowScan != null)
            {
                if (LowScan.IsChecked == true)
                {
                    LowScan.IsChecked = false;
                    //change the scripts database to high scan.
                    Database.setProtectionHigh();
                }
            }
        }

        void OnUnCheckHigh(object sender, RoutedEventArgs e)
        {
            if (LowScan != null)
            {
                if (LowScan.IsChecked == false)
                {
                    HighScan.IsChecked = true;
                }
            }
        }

        private void OnClickThreats(object sender, RoutedEventArgs e)
        {
            //move to the Threats & Warnings window.
            Warnings win2 = new Warnings();
            win2.Top = this.Top;
            win2.Left = this.Left;
            win2.Show();
            this.Close();
        }

        public void CheckThreats()
        {
            string stm;
            SQLiteDataReader rdr;
            while (true)
            {
                Dispatcher.Invoke(() =>
                {
                    if (isServerRunning && !Application.Current.Windows.OfType<ThreatJumper>().Any())
                    {
                        //load the database.
                        sqlite_conn = Database.CreateConnection();
                        stm = "SELECT * FROM Threats LIMIT 1";
                        var cmd = new SQLiteCommand(stm, this.sqlite_conn);
                        rdr = cmd.ExecuteReader();
                        if(rdr.Read())
                        {
                            rdr.Close();
                            sqlite_conn.Close();
                            ThreatJumper win2 = new ThreatJumper();
                            win2.Show();
                        }
                    }
                });
                Thread.Sleep(1000);
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            //do my stuff before closing
            t1.Abort();
            base.OnClosing(e);
        }
        private void OnWindowclose(object sender, EventArgs e)
        {
            Environment.Exit(Environment.ExitCode); // Prevent memory leak
        }

        private void OnClickStats(object sender, RoutedEventArgs e)
        {
            //move to the Site Stats window.
            SiteStats win2 = new SiteStats();
            win2.Top = this.Top;
            win2.Left = this.Left;
            win2.Show();
            this.Close();
        }

        private void OnClickChat(object sender, RoutedEventArgs e)
        {
            //move to the db window which show all the Threats History, and also buttons that change the db (add to blocked, white or remove from them).
            chatgpt win2 = new chatgpt();
            win2.Top = this.Top;
            win2.Left = this.Left;
            win2.Show();
            t1.Abort();
            this.Close();
        }
    }

    //help to find the mitmproxy parent (cmd) to close it.
    public struct ParentProcessUtilities
    {
        // These members must match PROCESS_BASIC_INFORMATION
        internal IntPtr Reserved1;
        internal IntPtr PebBaseAddress;
        internal IntPtr Reserved2_0;
        internal IntPtr Reserved2_1;
        internal IntPtr UniqueProcessId;
        internal IntPtr InheritedFromUniqueProcessId;

        [DllImport("ntdll.dll")]
        private static extern int NtQueryInformationProcess(IntPtr processHandle, int processInformationClass, ref ParentProcessUtilities processInformation, int processInformationLength, out int returnLength);

        /// <summary>
        /// Gets the parent process of the current process.
        /// </summary>
        /// <returns>An instance of the Process class.</returns>
        public static Process GetParentProcess()
        {
            return GetParentProcess(Process.GetCurrentProcess().Handle);
        }

        /// <summary>
        /// Gets the parent process of specified process.
        /// </summary>
        /// <param name="id">The process id.</param>
        /// <returns>An instance of the Process class.</returns>
        public static Process GetParentProcess(int id)
        {
            Process process = Process.GetProcessById(id);
            return GetParentProcess(process.Handle);
        }

        /// <summary>
        /// Gets the parent process of a specified process.
        /// </summary>
        /// <param name="handle">The process handle.</param>
        /// <returns>An instance of the Process class.</returns>
        public static Process GetParentProcess(IntPtr handle)
        {
            ParentProcessUtilities pbi = new ParentProcessUtilities();
            int returnLength;
            int status = NtQueryInformationProcess(handle, 0, ref pbi, Marshal.SizeOf(pbi), out returnLength);
            if (status != 0)
                throw new Win32Exception(status);

            try
            {
                return Process.GetProcessById(pbi.InheritedFromUniqueProcessId.ToInt32());
            }
            catch (ArgumentException)
            {
                // not found
                return null;
            }
        }
    }
}
