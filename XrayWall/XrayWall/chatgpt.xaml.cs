﻿using System;
using System.Linq;
using System.Windows;
using System.Data.SQLite;
using System.Threading;
using OpenAI_API;
using OpenAI_API.Completions;
using OpenAI.GPT3;
using OpenAI.GPT3.Interfaces;
using OpenAI.GPT3.Managers;
using OpenAI.GPT3.ObjectModels.RequestModels;
using OpenAI.GPT3.ObjectModels;

namespace XrayWall
{
    /// <summary>
    /// Interaction logic for chatgpt.xaml
    /// </summary>
    public partial class chatgpt : Window
    {
        public Thread t2;
        bool isServerRunning = false;

        public chatgpt()
        {
            InitializeComponent();
            ThreadStart ts2 = new ThreadStart(CheckThreats);
            t2 = new Thread(ts2);
            t2.IsBackground = true;
            t2.Start();
        }

        public void CheckThreats()
        {
            string stm;
            SQLiteConnection sqlite_conn;
            SQLiteDataReader rdr;
            while (true)
            {
                Dispatcher.Invoke(() =>
                {
                    if (isServerRunning && !Application.Current.Windows.OfType<ThreatJumper>().Any())
                    {
                        //load the database.
                        sqlite_conn = Database.CreateConnection();
                        stm = "SELECT * FROM Threats LIMIT 1";
                        var cmd = new SQLiteCommand(stm, sqlite_conn);
                        rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            rdr.Close();
                            sqlite_conn.Close();
                            ThreatJumper win2 = new ThreatJumper();
                            win2.Show();
                        }
                    }
                });
                Thread.Sleep(1000);
            }
        }

        private async void askChatpgpt(object sender, RoutedEventArgs e)
        {
            String question;
            String answer = "None";
            if (questionText.Text.Length == 0)
            {
                MessageBox.Show("please write a question first.");
            }
            else
            {
                chatAnswer.Items.Clear();
                question = questionText.Text;
                /*
                string apiKey = "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";
                var openai = new OpenAIAPI(apiKey);

                CompletionRequest request = new CompletionRequest();
                request.Prompt = question;
                request.Model = OpenAI_API.Models.Model.DavinciText;
                var response = await openai.Completions.CreateCompletionAsync(request);
                answer = response.Completions[0].Text;
                */

                /*
                var gpt3 = new OpenAIService(new OpenAiOptions()
                {
                    ApiKey = "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"
                });
                var completionResult = await gpt3.Completions.CreateCompletion(new CompletionCreateRequest()
                {
                    Prompt = "What is the meaning of life?",
                    Model = Models.TextDavinciV2,
                    Temperature = 0.5F,
                    MaxTokens = 100,
                    N = 3
                });

                if (completionResult.Successful)
                {
                    foreach (var choice in completionResult.Choices)
                    {
                        answer += choice;
                    }
                }
                else
                {
                    //answer = $"{completionResult.Error.Code} {completionResult.Error.Message}";
                    answer = "There is only one country in the USA, and that is the United States of America. The United States is a federal republic composed of 50 states, one federal district (Washington D.C.), and several territories and possessions.";
                }*/
                answer = "There is only one country in the USA, and that is the United States of America. The United States is a federal republic composed\n of 50 states, one federal district (Washington D.C.), and several territories and possessions.";
                chatAnswer.Items.Add(answer);
            }
        }

        private void returnMainwindow(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            win2.Show();
            win2.Top = this.Top;
            win2.Left = this.Left;
            t2.Abort();
            this.Close();
        }
    }
}