﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XrayWall
{
    class Warning
    {
        private String ThreatName;
        private String url;
        private String ip;
        private String Date;
        private String details;

        public Warning(String ThreatName, String url, String ip, String Date, String details)
        {
            this.ThreatName = ThreatName;
            this.url = url;
            this.ip = ip;
            this.Date = Date;
            this.details = details;
        }
        public String getThreatName()
        {
            return this.ThreatName;
        }
        public String getUrl()
        {
            return this.url;
        }
        public String getIp()
        {
            return this.ip;
        }
        public String getDate()
        {
            return this.Date;
        }
        public String getDetails()
        {
            return this.details;
        }
    }
}
