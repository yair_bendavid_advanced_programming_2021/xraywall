﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.SQLite;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Net;

namespace XrayWall
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Logs : Window
    {
        private static Mutex mutexHistory = new Mutex();
        private static Mutex mutexBlackList = new Mutex();
        private static Mutex mutexWhiteList = new Mutex();
        public Thread t1;
        public Thread t2;
        bool isServerRunning = false;
        public Logs()
        {
            InitializeComponent();
            Process[] pname = Process.GetProcessesByName("mitmproxy");
            if (pname.Length > 0)
            {
                isServerRunning = true;
            }
            ThreadStart ts1 = new ThreadStart(UpdateDataThread);
            t1 = new Thread(ts1);
            t1.IsBackground = true; // Run in background
            t1.Start();
            ThreadStart ts2 = new ThreadStart(CheckThreats);
            t2 = new Thread(ts2);
            t2.IsBackground = true;
            t2.Start();
        }
        public void CheckThreats()
        {
            string stm;
            SQLiteConnection sqlite_conn;
            SQLiteDataReader rdr;
            while (true)
            {
                Dispatcher.Invoke(() =>
                {
                    if (isServerRunning && !Application.Current.Windows.OfType<ThreatJumper>().Any())
                    {
                        //load the database.
                        sqlite_conn = Database.CreateConnection();
                        stm = "SELECT * FROM Threats LIMIT 1";
                        var cmd = new SQLiteCommand(stm, sqlite_conn);
                        rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            rdr.Close();
                            sqlite_conn.Close();
                            ThreatJumper win2 = new ThreatJumper();
                            win2.Show();
                        }
                    }
                });
                Thread.Sleep(1000);
            }
        }

        public void refreshWhitelist()
        {
            SQLiteConnection sqlite_conn;
            string stm;
            SQLiteDataReader rdr;
            WhitelistItems.SelectedItem = null;
            if (WhitelistItems.SelectedItem == null)
            {
                sqlite_conn = Database.CreateConnection();
                stm = "SELECT * FROM WhiteList";
                var cmd = new SQLiteCommand(stm, sqlite_conn);
                rdr = cmd.ExecuteReader();
                mutexWhiteList.WaitOne();
                WhitelistItems.Items.Clear();
                mutexWhiteList.ReleaseMutex();
                while (rdr.Read())
                {
                    mutexWhiteList.WaitOne();
                    WhitelistItems.Items.Add(rdr.GetString(2));
                    mutexWhiteList.ReleaseMutex();
                }
            }
            else
            {
                MessageBox.Show("Please stop select items bruh, let us refresh.");
            }
        }

        public void refreshBlacklist()
        {
            SQLiteConnection sqlite_conn;
            string stm;
            SQLiteDataReader rdr;
            BlacklistItems.SelectedItem = null;
            if (BlacklistItems.SelectedItem == null)
            {
                sqlite_conn = Database.CreateConnection();
                stm = "SELECT * FROM BlackList";
                var cmd = new SQLiteCommand(stm, sqlite_conn);
                rdr = cmd.ExecuteReader();
                mutexBlackList.WaitOne();
                BlacklistItems.Items.Clear();
                mutexBlackList.ReleaseMutex();
                while (rdr.Read())
                {
                    mutexBlackList.WaitOne();
                    BlacklistItems.Items.Add(rdr.GetString(2));
                    mutexBlackList.ReleaseMutex();
                }
            }
            else
            {
                MessageBox.Show("Please stop select items bruh, let us refresh.");
            }
        }

        public void UpdateDataThread()
        {
            SQLiteConnection sqlite_conn;
            string stm;
            SQLiteDataReader rdr;
            while (true)
            {
                //load the database.
                sqlite_conn = Database.CreateConnection();
                Dispatcher.Invoke(() =>
                {
                    if (HistoryItems.SelectedItem == null)
                    {
                        stm = "SELECT * FROM History";
                        var cmd = new SQLiteCommand(stm, sqlite_conn);
                        rdr = cmd.ExecuteReader();
                        mutexHistory.WaitOne();
                        HistoryItems.Items.Clear();
                        mutexHistory.ReleaseMutex();
                        while (rdr.Read())
                        {
                            mutexHistory.WaitOne();
                            HistoryItems.Items.Add(rdr.GetString(2));
                            mutexHistory.ReleaseMutex();
                        }
                    }
                });

                //load here all the listBoxes with the content.
                //history listBox: (get all the items of the table in the database and add them).
                //HistoryItems.Items.Add("log of XSS attack");

                //WhiteList listBox: (get all the items of the table in the database and add them).
                Dispatcher.Invoke(() =>
                {
                    if (WhitelistItems.SelectedItem == null)
                    {
                        stm = "SELECT * FROM WhiteList";
                        var cmd = new SQLiteCommand(stm, sqlite_conn);
                        rdr = cmd.ExecuteReader();
                        mutexWhiteList.WaitOne();
                        WhitelistItems.Items.Clear();
                        mutexWhiteList.ReleaseMutex();
                        while (rdr.Read())
                        {
                            mutexWhiteList.WaitOne();
                            WhitelistItems.Items.Add(rdr.GetString(2));
                            mutexWhiteList.ReleaseMutex();
                        }
                    }
                });

                //BlackList listBox: (get all the items of the table in the database and add them).
                Dispatcher.Invoke(() =>
                {
                    if (BlacklistItems.SelectedItem == null)
                    {
                        stm = "SELECT * FROM BlackList";
                        var cmd = new SQLiteCommand(stm, sqlite_conn);
                        rdr = cmd.ExecuteReader();
                        mutexBlackList.WaitOne();
                        BlacklistItems.Items.Clear();
                        mutexBlackList.ReleaseMutex();
                        while (rdr.Read())
                        {
                            mutexBlackList.WaitOne();
                            BlacklistItems.Items.Add(rdr.GetString(2));
                            mutexBlackList.ReleaseMutex();
                        }
                    }
                });
                Thread.Sleep(1000);
            }
        }


        /*
        Info:
        Btn width: 220.
        Btn Heigth: 85.
        ----------------------
        Actions:
        return to the main window.
        */
        private void returnMainWindow(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            win2.Show();
            win2.Top = this.Top;
            win2.Left = this.Left;
            t1.Abort();
            t2.Abort();
            this.Close();
        }

        /*
        Info:
        Btn width: 220.
        Btn Heigth: 85.
        ----------------------
        Actions:
        the button clear the History table in the database.
        (update the database).
        (clear the itemList in the gui).
        */
        private void ClearHistory(object sender, RoutedEventArgs e)
        {
            //clear all the history of the listbox and also clear the history table in the database.
            SQLiteConnection sqlite_conn = Database.CreateConnection();

            string sqlStatement = "DELETE FROM History";

            try
            {
                SQLiteCommand cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                cmd.ExecuteNonQuery();

            }
            finally
            {
                sqlite_conn.Close();
            }
            mutexHistory.WaitOne();
            HistoryItems.Items.Clear();
            mutexHistory.ReleaseMutex();
        }

        /*
        Info:
        Btn width: 220.
        Btn Heigth: 85.
        ----------------------
        Actions:
        the button add new address (ip, url) to the whiteList table in the database.
        (update the database).
        (add the new item to the itemList in the gui).
        */
        private void AddWhitelist(object sender, RoutedEventArgs e)
        {
            if(urlText.Text.Length > 0) //means the user entered something to the textBox.
            {
                if(urlText.Text.ToLower().Contains("https://") || urlText.Text.ToLower().Contains("http://")) //make sure it contains a valid url
                {
                    if (!Database.getIsWhitelist(urlText.Text))
                    {
                        //make sure that url actually exist.
                        WaitWindowHandler w2 = new WaitWindowHandler();
                        w2.Start(this.Top, this.Left);
                        if (UrlIsValid(urlText.Text))
                        {
                            w2.Stop();
                            Uri myUri = new Uri(urlText.Text);
                            var ip = Dns.GetHostAddresses(myUri.Host)[0];
                            if (!Database.getIsWhitelistIp(ip.ToString()))
                            {
                                SQLiteConnection sqlite_conn = Database.CreateConnection();
                                string sqlStatement;
                                SQLiteCommand cmd;
                                DateTime now = DateTime.Now;
                                //now we shall add it to the whitelist database after we made sure  the url is valid and that he clicked the add of the white list.
                                sqlStatement = "insert into WhiteList (ThreatName, URL, IP, DATE) values ('Added by user','" + urlText.Text + "','" + ip.ToString() + "','" + now.ToString() + "');";
                                cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                                cmd.ExecuteNonQuery();
                                //now check if it in Blacklist so we should remove it from there if it is.
                                if (Database.getIsBlacklist(urlText.Text.ToString()) || Database.getIsBlacklistIp(ip.ToString())) //we shall remove it from there.
                                {
                                    if (!Database.removeBlackListUrl(urlText.Text.ToString()))
                                    {
                                        if (!Database.removeBlackListIp(ip.ToString()))
                                        {
                                            MessageBox.Show("Failed to remove from black list! (maybe removed by proxy)");
                                        }
                                        else
                                        {
                                            refreshBlacklist();
                                        }
                                    }
                                    else
                                    {
                                        refreshBlacklist();
                                    }
                                }
                                else
                                {
                                    sqlite_conn.Close();
                                }
                            }
                            else
                            {
                                MessageBox.Show("This url is already in the WhiteList...");
                            }
                        }
                        else
                        {
                            w2.Stop();
                            MessageBox.Show("Error: (domain not exist / cant reach the requested domain / timeout)");
                        }
                    }
                    else
                    {
                        MessageBox.Show("This url is already in the WhiteList...");
                    }
                }
                else
                {
                    MessageBox.Show("invalid url format (try using https:// in the start)");
                }
            }
            else
            {
                MessageBox.Show("You should enter a url first...");
            }
        }

        /*
        Info:
        Btn width: 220.
        Btn Heigth: 85.
        ----------------------
        Actions:
        the button Delete Existed address from the whiteList table in the database (by click on item in the ItemList and then on Delete or by value).
        (update the database).
        (delete the item from the itemList in the gui).
        */
        private void DeleteWhitelist(object sender, RoutedEventArgs e)
        {
            if(WhitelistItems.SelectedItem != null)
            {
                SQLiteConnection sqlite_conn = Database.CreateConnection();

                mutexWhiteList.WaitOne();
                string sqlStatement = "DELETE FROM WhiteList WHERE URL = " + "\"" + WhitelistItems.SelectedItem.ToString() + "\"";
                mutexWhiteList.ReleaseMutex();
                try
                {
                    SQLiteCommand cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                    cmd.ExecuteNonQuery();

                }
                finally
                {
                    sqlite_conn.Close();
                }
                mutexWhiteList.WaitOne();
                WhitelistItems.Items.Remove(WhitelistItems.SelectedItem);
                mutexWhiteList.ReleaseMutex();
            }
        }

        /*
        Info:
        Btn width: 220.
        Btn Heigth: 85.
        ----------------------
        Actions:
        the button add new address (ip, url) to the BlackList table in the database.
        (update the database).
        (add the new item to the itemList in the gui).
        */
        private void AddBlacklist(object sender, RoutedEventArgs e)
        {
            if (urlText.Text.Length > 0) //means the user entered something to the textBox.
            {
                if (urlText.Text.ToLower().Contains("https://") || urlText.Text.ToLower().Contains("http://")) //make sure it contains a valid url
                {
                    if (!Database.getIsBlacklist(urlText.Text.ToString()))
                    {
                        //make sure that url actually exist.
                        WaitWindowHandler w2 = new WaitWindowHandler();
                        w2.Start(this.Top, this.Left);
                        if (UrlIsValid(urlText.Text))
                        {
                            w2.Stop();
                            Uri myUri = new Uri(urlText.Text);
                            var ip = Dns.GetHostAddresses(myUri.Host)[0];
                            if(!Database.getIsBlacklistIp(ip.ToString()))
                            {
                                SQLiteConnection sqlite_conn = Database.CreateConnection();
                                string sqlStatement;
                                SQLiteCommand cmd;
                                DateTime now = DateTime.Now;
                                //now we shall add it to the whitelist database after we made sure  the url is valid and that he clicked the add of the white list.
                                sqlStatement = "insert into BlackList (ThreatName, URL, IP, DATE) values ('Added by user','" + urlText.Text + "','" + ip.ToString() + "','" + now.ToString() + "');";
                                cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                                cmd.ExecuteNonQuery();
                                //now check if it in whitelist so we should remove it from there if it is.
                                if(Database.getIsWhitelist(urlText.Text.ToString()) || Database.getIsWhitelistIp(ip.ToString())) //we shall remove it from there.
                                {
                                    if(!Database.removeWhiteListUrl(urlText.Text.ToString()))
                                    {
                                        if(!Database.removeWhiteListIp(ip.ToString()))
                                        {
                                            MessageBox.Show("Failed to remove from white list! (maybe removed by proxy)");
                                        }
                                        else
                                        {
                                            refreshWhitelist();
                                        }
                                    }
                                    else
                                    {
                                        refreshWhitelist();
                                    }
                                }
                                else
                                {
                                    sqlite_conn.Close();
                                }
                            }
                            else
                            {
                                MessageBox.Show("This url is already in the BlackList...");
                            }
                        }
                        else
                        {
                            w2.Stop();
                            MessageBox.Show("Error: (domain not exist / cant reach the requested domain / timeout)");
                        }
                    }
                    else
                    {
                        MessageBox.Show("This url is already in the BlackList...");
                    }
                }
                else
                {
                    MessageBox.Show("invalid url format (try using https:// in the start)");
                }
            }
            else
            {
                MessageBox.Show("You should enter a url first...");
            }
        }

        /*
        Info:
        Btn width: 220.
        Btn Heigth: 85.
        ----------------------
        Actions:
        the button Delete Existed address from the BlackList table in the database (by click on item in the ItemList and then on Delete or by value).
        (update the database).
        (delete the item from the itemList in the gui).
        */
        private void DeleteBlacklist(object sender, RoutedEventArgs e)
        {
            if(BlacklistItems.SelectedItem != null)
            {
                //update also the databse (remove the item also from the database).
                SQLiteConnection sqlite_conn = Database.CreateConnection();

                mutexBlackList.WaitOne();
                string sqlStatement = "DELETE FROM BlackList WHERE URL = " + "\"" + BlacklistItems.SelectedItem.ToString() + "\"";
                mutexBlackList.ReleaseMutex();
                try
                {
                    SQLiteCommand cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                    cmd.ExecuteNonQuery();

                }
                finally
                {
                    sqlite_conn.Close();
                }
                mutexBlackList.WaitOne();
                BlacklistItems.Items.Remove(BlacklistItems.SelectedItem);
                mutexBlackList.ReleaseMutex();
            }
        }
        private void OnWindowclose(object sender, EventArgs e)
        {
            t1.Abort();
            t2.Abort();
            Environment.Exit(Environment.ExitCode); // Prevent memory leak
        }

        private void ShowDetails(object sender, RoutedEventArgs e)
        {
            if (HistoryItems.SelectedItem != null)
            {
                History h = Database.getHistory(HistoryItems.SelectedItem.ToString());
                MessageBox.Show("Threat Name: " + h.getThreatName() + "\nUrl: " + h.getUrl() + "\nIP: " + h.getIp() + "\nDate: " + h.getDate() + "\nDetails: " + h.getDetails());
            }
            else if (WhitelistItems.SelectedItem != null)
            {
                WhiteList w = Database.getWhiteList(WhitelistItems.SelectedItem.ToString());
                MessageBox.Show("Threat Name: " + w.getThreatName() + "\nUrl: " + w.getUrl() + "\nIP: " + w.getIp() + "\nDate: " + w.getDate());
            }
            else if(BlacklistItems.SelectedItem != null)
            {
                BlackList b = Database.getBlackList(BlacklistItems.SelectedItem.ToString());
                MessageBox.Show("Threat Name: " + b.getThreatName() + "\nUrl: " + b.getUrl() + "\nIP: " + b.getIp() + "\nDate: " + b.getDate());
            }
        }
        /// <summary>
        /// This method will check a url to see that it does not return server or protocol errors
        /// </summary>
        /// <param name="url">The path to check</param>
        /// <returns></returns>
        public bool UrlIsValid(string url)
        {
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
                request.Timeout = 5000;
                request.Method = "HEAD"; //Get only the header information -- no need to download any content

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    int statusCode = (int)response.StatusCode;
                    if (statusCode >= 100 && statusCode < 400) //Good requests
                    {
                        return true;
                    }
                    else if (statusCode >= 500 && statusCode <= 510) //Server Errors
                    {
                        return false;
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError) //400 errors
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }
    }
}
