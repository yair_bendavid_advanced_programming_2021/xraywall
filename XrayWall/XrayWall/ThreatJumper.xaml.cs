﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace XrayWall
{
    /// <summary>
    /// Interaction logic for ThreatJumper.xaml
    /// </summary>
    public partial class ThreatJumper : Window
    {
        Threats t;
        SQLiteConnection sqlite_conn;

        public ThreatJumper()
        {
            InitializeComponent();
            sqlite_conn = Database.CreateConnection();
            t = Database.getThreats();
            //set all the Text block info.
            ThreatName.Text = "Threat Name: " + t.getThreatName();
            ThreatUrl.Text = "Threat Url: " + t.getUrl();
            ThreatIp.Text = "Threat IP: " + t.getIp();
            ThreatDate.Text = "Threat Date: " + t.getDate();
            ThreatDetails.Text = "Threat Details: " + t.getDetails();
        }

        /*
         Button info:
         width - 120.
         height - 85.

         Function info:
         close the threat window and remove the threat from the database.
        */
        private void OnClickIgnore(object sender, RoutedEventArgs e)
        {
            string sqlStatement = "DELETE FROM Threats WHERE URL = " + "\"" + t.getUrl() + "\"";
            SQLiteCommand cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "DELETE FROM BlackList WHERE URL = " + "\"" + t.getUrl() + "\"";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlite_conn.Close();
            this.Close();
        }

        /*
         Button info:
         width - 120.
         height - 85.

         Function info:
         close the threat window, remove the threat from the database and add the threat to the blockList table.
        */
        private void OnClickBlock(object sender, RoutedEventArgs e)
        {
            string sqlStatement = "DELETE FROM Threats WHERE URL = " + "\"" + t.getUrl() + "\"";
            SQLiteCommand cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlite_conn.Close();
            this.Close();
        }

        /*
         Button info:
         width - 120.
         height - 85.

         Function info:
         close the threat window, remove the threat from the database and the threat to the whiteList table.
        */
        private void OnClickTrust(object sender, RoutedEventArgs e)
        {
            string sqlStatement = "DELETE FROM Threats WHERE URL = " + "\"" + t.getUrl() + "\"";
            SQLiteCommand cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "DELETE FROM BlackList WHERE URL = " + "\"" + t.getUrl() + "\"";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into WhiteList (ThreatName, URL, IP, DATE) values ('" + t.getThreatName() + "','"  + t.getUrl() + "','" + t.getIp() + "','" + t.getDate() + "');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlite_conn.Close();
            this.Close();
        }
    }
}
