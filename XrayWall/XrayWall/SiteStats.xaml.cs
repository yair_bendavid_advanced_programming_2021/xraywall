﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.SQLite;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Threading;

namespace XrayWall
{
    /// <summary>
    /// Interaction logic for SiteStats.xaml
    /// </summary>
    public partial class SiteStats : Window
    {
        public Thread t2;
        bool isServerRunning = false;

        public SiteStats()
        {
            InitializeComponent();
            ThreadStart ts2 = new ThreadStart(CheckThreats);
            t2 = new Thread(ts2);
            t2.IsBackground = true;
            t2.Start();
        }

        public void CheckThreats()
        {
            string stm;
            SQLiteConnection sqlite_conn;
            SQLiteDataReader rdr;
            while (true)
            {
                Dispatcher.Invoke(() =>
                {
                    if (isServerRunning && !Application.Current.Windows.OfType<ThreatJumper>().Any())
                    {
                        //load the database.
                        sqlite_conn = Database.CreateConnection();
                        stm = "SELECT * FROM Threats LIMIT 1";
                        var cmd = new SQLiteCommand(stm, sqlite_conn);
                        rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            rdr.Close();
                            sqlite_conn.Close();
                            ThreatJumper win2 = new ThreatJumper();
                            win2.Show();
                        }
                    }
                });
                Thread.Sleep(1000);
            }
        }

        private void OnCheckStats(object sender, RoutedEventArgs e)
        {
            if (urlText.Text.Length > 0) //means the user entered something to the textBox.
            {
                if (urlText.Text.ToLower().Contains("https://") || urlText.Text.ToLower().Contains("http://")) //make sure it contains a valid url
                {
                    //make sure that url actually exist.
                    WaitWindowHandler w = new WaitWindowHandler();
                    w.Start(this.Top, this.Left);
                    if (UrlIsValid(urlText.Text))
                    {
                        w.Stop();
                        //set url.
                        SiteUrl.Text = "URL: " + urlText.Text;
                        //set ip.
                        Uri myUri = new Uri(urlText.Text);
                        var ip = Dns.GetHostAddresses(myUri.Host)[0];
                        SiteIp.Text = "IP: " + ip.ToString();
                        //set protocol.
                        var protocol = myUri.Scheme;
                        SiteProtocol.Text = "Protocol: " + protocol.ToString();
                        //now check if there are any records of this site in History table and the count of them (check for count in sql).
                        SitePastThreats.Text = "Past Threats: " + Database.getHistoryCount(urlText.Text.ToString(), ip.ToString()).ToString();
                        //check if the url is in the blackList.
                        if(Database.getIsBlacklist(urlText.Text.ToString()) || Database.getIsBlacklistIp(ip.ToString()))
                        {
                            SiteIsBlocked.Text = "Is Blocked: Yes";
                            SiteIsTrusted.Text = "Is Trusted: No";

                        }
                        else //check if the url is in the WhiteList (dont need if it is in the black list).
                        {
                            SiteIsBlocked.Text = "Is Blocked: No";
                            if (Database.getIsWhitelist(urlText.Text.ToString()) || Database.getIsWhitelistIp(ip.ToString()))
                            {
                                SiteIsTrusted.Text = "Is Trusted: Yes";
                            }
                            else
                            {
                                SiteIsTrusted.Text = "Is Trusted: No";
                            }
                        }
                    }
                    else
                    {
                        w.Stop();
                        MessageBox.Show("Error: (domain not exist / cant reach the requested domain / timeout)");
                    }
                }
                else
                {
                    MessageBox.Show("invalid url format (try using https:// in the start)");
                }
            }
            else
            {
                MessageBox.Show("You should enter a url first...");
            }

        }

        /// <summary>
        /// This method will check a url to see that it does not return server or protocol errors
        /// </summary>
        /// <param name="url">The path to check</param>
        /// <returns></returns>
        public bool UrlIsValid(string url)
        {
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
                request.Timeout = 4000;
                request.Method = "HEAD"; //Get only the header information -- no need to download any content

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    int statusCode = (int)response.StatusCode;
                    if (statusCode >= 100 && statusCode < 400) //Good requests
                    {
                        return true;
                    }
                    else if (statusCode >= 500 && statusCode <= 510) //Server Errors
                    {
                        return false;
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError) //400 errors
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        private void OnReturnMain(object sender, RoutedEventArgs e)
        {
            MainWindow w2 = new MainWindow();
            w2.Top = this.Top;
            w2.Left = this.Left;
            w2.Show();
            t2.Abort();
            this.Close();
        }
    }
}
