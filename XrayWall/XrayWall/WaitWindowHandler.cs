﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using System.Threading.Tasks;

namespace XrayWall
{
	public class WaitWindowHandler
	{

		private Thread StatusThread = null;

		private WaitWindow Popup = null;

		public void Start(double top, double left)
		{
			//create the thread with its ThreadStart method
			this.StatusThread = new Thread(() =>
			{
				try
				{
					this.Popup = new WaitWindow();
					this.Popup.Top = top;
					this.Popup.Left = left;
					this.Popup.Show();
					
					this.Popup.Closed += (lsender, le) =>
					{
						//when the window closes, close the thread invoking the shutdown of the dispatcher
						this.Popup.Dispatcher.InvokeShutdown();
						this.Popup = null;
						this.StatusThread = null;
					};

					//this call is needed so the thread remains open until the dispatcher is closed
					System.Windows.Threading.Dispatcher.Run();
				}
				catch (Exception ex)
				{
				}
			});

			//run the thread in STA mode to make it work correctly
			this.StatusThread.SetApartmentState(ApartmentState.STA);
			this.StatusThread.Priority = ThreadPriority.Normal;
			this.StatusThread.Start();
		}

		public void Stop()
		{
			if (this.Popup != null)
			{
				//need to use the dispatcher to call the Close method, because the window is created in another thread, and this method is called by the main thread
				this.Popup.Dispatcher.BeginInvoke(new Action(() =>
				{
					this.Popup.Close();
				}));
			}
		}
	}
}
