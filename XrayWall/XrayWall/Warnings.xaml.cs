﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.SQLite;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Diagnostics;

namespace XrayWall
{
    /// <summary>
    /// Interaction logic for Warnings.xaml
    /// </summary>
    public partial class Warnings : Window
    {
        private static Mutex mutexWarnings = new Mutex();
        public Thread t1;
        public Thread t2;
        bool isServerRunning = false;

        public Warnings()
        {
            InitializeComponent();
            Process[] pname = Process.GetProcessesByName("mitmproxy");
            if (pname.Length > 0)
            {
                isServerRunning = true;
            }
            ThreadStart ts1 = new ThreadStart(UpdateDataThread);
            t1 = new Thread(ts1);
            t1.IsBackground = true; // Run in background
            t1.Start();
            ThreadStart ts2 = new ThreadStart(CheckThreats);
            t2 = new Thread(ts2);
            t2.IsBackground = true;
            t2.Start();
        }

        public void CheckThreats()
        {
            string stm;
            SQLiteConnection sqlite_conn;
            SQLiteDataReader rdr;
            while (true)
            {
                Dispatcher.Invoke(() =>
                {
                    if (isServerRunning && !Application.Current.Windows.OfType<ThreatJumper>().Any())
                    {
                        //load the database.
                        sqlite_conn = Database.CreateConnection();
                        stm = "SELECT * FROM Threats LIMIT 1";
                        var cmd = new SQLiteCommand(stm, sqlite_conn);
                        rdr = cmd.ExecuteReader();
                        if (rdr.Read())
                        {
                            rdr.Close();
                            sqlite_conn.Close();
                            ThreatJumper win2 = new ThreatJumper();
                            win2.Show();
                        }
                    }
                });
                Thread.Sleep(1000);
            }
        }
        public void UpdateDataThread()
        {
            SQLiteConnection sqlite_conn;
            string stm;
            SQLiteDataReader rdr;
            while (true)
            {
                //load the database.
                sqlite_conn = Database.CreateConnection();
                Dispatcher.Invoke(() =>
                {
                    if (WarningsItems.SelectedItem == null)
                    {
                        stm = "SELECT * FROM Warnings";
                        var cmd = new SQLiteCommand(stm, sqlite_conn);
                        rdr = cmd.ExecuteReader();
                        mutexWarnings.WaitOne();
                        WarningsItems.Items.Clear();
                        mutexWarnings.ReleaseMutex();
                        while (rdr.Read())
                        {
                            mutexWarnings.WaitOne();
                            WarningsItems.Items.Add(rdr.GetString(2));
                            mutexWarnings.ReleaseMutex();
                        }
                    }
                });
                Thread.Sleep(1000);
            }
        }

        private void BlockWarning(object sender, RoutedEventArgs e)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string sqlStatement;
            SQLiteCommand cmd;
            mutexWarnings.WaitOne();
            sqlStatement = "DELETE FROM Warnings WHERE URL = " + "\"" + WarningsItems.SelectedItem.ToString() + "\"";
            mutexWarnings.ReleaseMutex();
            try
            {
                cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
            DateTime now = DateTime.Now;
            mutexWarnings.WaitOne();
            Uri myUri = new Uri(WarningsItems.SelectedItem.ToString());
            var ip = Dns.GetHostAddresses(myUri.Host)[0];
            sqlStatement = "insert into BlackList (ThreatName, URL, IP, DATE) values ('Blocked Warning','" + WarningsItems.SelectedItem.ToString() + "','" + ip.ToString() + "','" + now.ToString() + "');";
            mutexWarnings.ReleaseMutex();
            try
            {
                cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                cmd.ExecuteNonQuery();

            }
            finally
            {
                sqlite_conn.Close();
            }
        }

        private void DeleteWarning(object sender, RoutedEventArgs e)
        {
            if (WarningsItems.SelectedItem != null)
            {
                //update also the databse (remove the item also from the database).
                SQLiteConnection sqlite_conn = Database.CreateConnection();

                mutexWarnings.WaitOne();
                string sqlStatement = "DELETE FROM Warnings WHERE URL = " + "\"" + WarningsItems.SelectedItem.ToString() + "\"";
                mutexWarnings.ReleaseMutex();
                try
                {
                    SQLiteCommand cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                    cmd.ExecuteNonQuery();

                }
                finally
                {
                    sqlite_conn.Close();
                }
                mutexWarnings.WaitOne();
                WarningsItems.Items.Remove(WarningsItems.SelectedItem);
                mutexWarnings.ReleaseMutex();
            }
        }

        private void WarningDetails(object sender, RoutedEventArgs e)
        {
            if(WarningsItems.SelectedItem != null)
            {
                Warning w = Database.getWarning(WarningsItems.SelectedItem.ToString());
                WarningsInfo winInfo = new WarningsInfo();
                winInfo.Top = this.Top;
                winInfo.Left = this.Left;
                String anotherSite = "";
                String unsafeSite = "";
                String[] anotherSiteLinks = { };
                String[] unsafeSiteLinks = { };
                if (w.getDetails().Contains("Links to another sites:"))
                {
                    if (w.getDetails().Contains("unsafe sites:")) //both unsafe and another sites.
                    {
                        anotherSite = w.getDetails().Substring(24, w.getDetails().IndexOf("unsafe sites:") - 25);
                        unsafeSite = w.getDetails().Substring(w.getDetails().IndexOf("unsafe sites:") + 14, w.getDetails().Length - (w.getDetails().IndexOf("unsafe sites:") + 14));
                        anotherSiteLinks = anotherSite.Split(' ', '\n');
                        unsafeSiteLinks = unsafeSite.Split(' ', '\n');
                    }
                    else //just another sites.
                    {
                        anotherSite = w.getDetails().Substring(24, w.getDetails().Length - 25);
                        anotherSiteLinks = anotherSite.Split(' ', '\n');
                    }
                }
                else //only unsafe sites.
                {
                    unsafeSite = w.getDetails().Substring(14, w.getDetails().Length - 15);
                    unsafeSiteLinks = unsafeSite.Split(' ', '\n');
                }
                winInfo.setWarning("Threat Name: " + w.getThreatName(), "Url: " + w.getUrl(), "IP: " + w.getIp(), "Date: " + w.getDate(), anotherSiteLinks, unsafeSiteLinks);
                winInfo.Show();
            }
            else
            {
                MessageBox.Show("You need to select an warning first.");
            }
        }

        private void OnWindowclose(object sender, EventArgs e)
        {
            t1.Abort();
            t2.Abort();
            Environment.Exit(Environment.ExitCode); // Prevent memory leak
        }

        private void ReturnMain(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            win2.Show();
            win2.Top = this.Top;
            win2.Left = this.Left;
            t1.Abort();
            t2.Abort();
            this.Close();
        }
    }
}
