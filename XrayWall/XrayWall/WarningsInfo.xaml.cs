﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace XrayWall
{
    /// <summary>
    /// Interaction logic for WarningsInfo.xaml
    /// </summary>
    public partial class WarningsInfo : Window
    {
        public WarningsInfo()
        {
            InitializeComponent();
        }

        public void setWarning(String Tname, String Turl, String Tip, String Tdate, String[] Tsafe, String[] Tunsafe)
        {
            ThreatName.Text = Tname;
            ThreatUrl.Text = Turl;
            ThreatIp.Text = Tip;
            ThreatDate.Text = Tdate;
            for(int i = 0; i < Tsafe.Length; i++)
            {
                safeSites.Items.Add(Tsafe[i]);
            }
            for (int i = 0; i < Tunsafe.Length; i++)
            {
                unsafeSites.Items.Add(Tunsafe[i]);
            }
        }

        private void ReturnMain(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
