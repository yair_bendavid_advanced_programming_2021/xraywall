﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace XrayWall
{
    static class Database
    {
        public static SQLiteConnection CreateConnection()
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            string cs = @"URI=file:C:\Users\user\mitmp\database.db";
            sqlite_conn = new SQLiteConnection(cs);
            // Open the connection:
            try
            {
                sqlite_conn.Open();
                using (SQLiteCommand command = sqlite_conn.CreateCommand())
                {
                    SQLiteCommand sqliteCmd = sqlite_conn.CreateCommand();
                    sqliteCmd.CommandText = "CREATE TABLE IF NOT EXISTS BlackList (ID INT Primary Key, ThreatName TEXT, URL TEXT, IP TEXT, DATE TEXT)";
                    sqliteCmd.ExecuteNonQuery();
                    sqliteCmd.CommandText = "CREATE TABLE IF NOT EXISTS WhiteList (ID INT Primary Key, ThreatName TEXT, URL TEXT, IP TEXT, DATE TEXT)";
                    sqliteCmd.ExecuteNonQuery();
                    sqliteCmd.CommandText = "CREATE TABLE IF NOT EXISTS History (ID INT Primary Key, ThreatName TEXT, URL TEXT, IP TEXT, DATE TEXT, Details TEXT)";
                    sqliteCmd.ExecuteNonQuery();
                    sqliteCmd.CommandText = "CREATE TABLE IF NOT EXISTS Warnings (ID INT Primary Key, ThreatName TEXT, URL TEXT, IP TEXT, DATE TEXT, Details TEXT)";
                    sqliteCmd.ExecuteNonQuery();
                    sqliteCmd.CommandText = "CREATE TABLE IF NOT EXISTS Threats (ID INT Primary Key, ThreatName TEXT, URL TEXT, IP TEXT, DATE TEXT, Details TEXT)";
                    sqliteCmd.ExecuteNonQuery();
                    sqliteCmd.CommandText = "CREATE TABLE IF NOT EXISTS URLS (ID INT Primary Key, URL TEXT, SCANNED TEXT)";
                    sqliteCmd.ExecuteNonQuery();
                    sqliteCmd.CommandText = "CREATE TABLE IF NOT EXISTS Scripts (Script TEXT, Details TEXT)";
                    sqliteCmd.ExecuteNonQuery();
                    sqliteCmd.CommandText = "CREATE TABLE IF NOT EXISTS DatabaseSettings (ProtectionLevel TEXT)";
                    sqliteCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {

            }
            return sqlite_conn;
        }

        public static bool isProtectionLow()
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string stm;
            int RowCount;

            stm = "SELECT Count(*) FROM DatabaseSettings WHERE ProtectionLevel='low' COLLATE NOCASE";
            var cmd = new SQLiteCommand(stm, sqlite_conn);

            RowCount = Convert.ToInt32(cmd.ExecuteScalar());
            if (RowCount > 0)
            {
                return true;
            }
            return false;
        }

        public static bool isProtectionHigh()
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string stm;
            int RowCount;

            stm = "SELECT Count(*) FROM DatabaseSettings WHERE ProtectionLevel='high' COLLATE NOCASE";
            var cmd = new SQLiteCommand(stm, sqlite_conn);

            RowCount = Convert.ToInt32(cmd.ExecuteScalar());
            if (RowCount > 0)
            {
                return true;
            }
            return false;
        }

        public static void setProtectionLow()
        {
            //just remove all the SCRIPTS table value if there are any.
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string sqlStatement;
            SQLiteCommand cmd;
            //set the protection level to high in the db settings.
            sqlStatement = "DELETE FROM DatabaseSettings";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into DatabaseSettings (ProtectionLevel) values ('low');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            //change the Scripts db to be low (clear it since we gonna Threat every script and not just specific ones).
            sqlStatement = "DELETE FROM Scripts";

            try
            {
                cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                cmd.ExecuteNonQuery();

            }
            finally
            {
                sqlite_conn.Close();
            }
        }

        public static void setProtectionHigh()
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string sqlStatement;
            SQLiteCommand cmd;
            //set the protection level to high in the db settings.
            sqlStatement = "DELETE FROM DatabaseSettings";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into DatabaseSettings (ProtectionLevel) values ('high');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            //now we shall add it to the whitelist database after we made sure  the url is valid and that he clicked the add of the white list.
            sqlStatement = "insert into Scripts (Script, Details) values ('document.cookie','a possible attempt to get your cookie!');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into Scripts (Script, Details) values ('http','a possible attempt to redirect to unsafe site!');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into Scripts (Script, Details) values ('window.location','a possible attempt to redirect!');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into Scripts (Script, Details) values ('window.history.back','a possible attempt to redirect to previous pages in history!');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into Scripts (Script, Details) values ('window.history.go','a possible attempt to redirect to previous pages in history!');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into Scripts (Script, Details) values ('window.navigate','a possible attempt to redirect!');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into Scripts (Script, Details) values ('document.location.href','a possible attempt to redirect!');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlStatement = "insert into Scripts (Script, Details) values ('document.location.href','a possible attempt to redirect!');";
            cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
            cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }

        public static History getHistory(String byUrl)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            String ThreatName;
            String url;
            String ip;
            String date;
            String details;
            History h;
            string stm;
            SQLiteDataReader rdr;
            stm = "SELECT * FROM History WHERE URL='" + byUrl + "' LIMIT 1";
            var cmd = new SQLiteCommand(stm, sqlite_conn);
            rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                ThreatName = rdr.GetString(1);
                url = rdr.GetString(2);
                ip = rdr.GetString(3);
                date = rdr.GetString(4);
                details = rdr.GetString(5);
                rdr.Close();
                h = new History(ThreatName, url, ip, date, details);
                return h;
            }
            else
            {
                h = new History("not in database", "not in database", "not in database", "not in database", "not in database");
                return h;
            }
        }

        public static int getHistoryCount(String byUrl, String byIp)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string stm;
            int RowCount = 0;

            stm = "SELECT Count(*) FROM History WHERE URL='" + byUrl.ToLower() + "' COLLATE NOCASE";
            var cmd = new SQLiteCommand(stm, sqlite_conn);

            RowCount = Convert.ToInt32(cmd.ExecuteScalar());
            if(RowCount == 0) //check by ip cus the url may be different (since its not by host name).
            {
                stm = "SELECT Count(*) FROM History WHERE IP='" + byIp.ToLower() + "' COLLATE NOCASE";
                cmd = new SQLiteCommand(stm, sqlite_conn);
                RowCount = Convert.ToInt32(cmd.ExecuteScalar());
            }
            return RowCount;
        }

        public static bool getIsBlacklist(String byUrl)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string stm;
            int RowCount;

            stm = "SELECT Count(*) FROM BlackList WHERE URL='" + byUrl.ToLower() + "' COLLATE NOCASE";
            var cmd = new SQLiteCommand(stm, sqlite_conn);

            RowCount = Convert.ToInt32(cmd.ExecuteScalar());
            if(RowCount > 0)
            {
                return true;
            }
            return false;
        }

        public static bool getIsBlacklistIp(String byIp)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string stm;
            int RowCount;

            stm = "SELECT Count(*) FROM BlackList WHERE IP='" + byIp.ToLower() + "' COLLATE NOCASE";
            var cmd = new SQLiteCommand(stm, sqlite_conn);

            RowCount = Convert.ToInt32(cmd.ExecuteScalar());
            if (RowCount > 0)
            {
                return true;
            }
            return false;
        }

        public static bool getIsWhitelist(String byUrl)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string stm;
            int RowCount;

            stm = "SELECT Count(*) FROM WhiteList WHERE URL='" + byUrl.ToLower() + "' COLLATE NOCASE";
            var cmd = new SQLiteCommand(stm, sqlite_conn);

            RowCount = Convert.ToInt32(cmd.ExecuteScalar());
            if (RowCount > 0)
            {
                return true;
            }
            return false;
        }

        public static bool getIsWhitelistIp(String byIp)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            string stm;
            int RowCount;

            stm = "SELECT Count(*) FROM WhiteList WHERE IP='" + byIp.ToLower() + "' COLLATE NOCASE";
            var cmd = new SQLiteCommand(stm, sqlite_conn);

            RowCount = Convert.ToInt32(cmd.ExecuteScalar());
            if (RowCount > 0)
            {
                return true;
            }
            return false;
        }

        public static bool removeBlackListUrl(String byUrl)
        {
            if(getIsBlacklist(byUrl))
            {
                SQLiteConnection sqlite_conn = Database.CreateConnection();
                string sqlStatement;
                sqlStatement = "DELETE FROM BlackList WHERE URL = " + "\"" + byUrl.ToLower() + "\" COLLATE NOCASE";
                try
                {
                    var cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    sqlite_conn.Close();
                }
                return true;
            }
            return false;
        }

        public static bool removeBlackListIp(String byIp)
        {
            if (getIsBlacklistIp(byIp))
            {
                SQLiteConnection sqlite_conn = Database.CreateConnection();
                string sqlStatement;
                sqlStatement = "DELETE FROM BlackList WHERE IP = " + "\"" + byIp.ToLower() + "\" COLLATE NOCASE";
                try
                {
                    var cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    sqlite_conn.Close();
                }
                return true;
            }
            return false;
        }

        public static bool removeWhiteListUrl(String byUrl)
        {
            if (getIsWhitelist(byUrl))
            {
                SQLiteConnection sqlite_conn = Database.CreateConnection();
                string sqlStatement;
                sqlStatement = "DELETE FROM WhiteList WHERE URL = " + "\"" + byUrl.ToLower() + "\" COLLATE NOCASE";
                try
                {
                    var cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    sqlite_conn.Close();
                }
                return true;
            }
            return false;
        }

        public static bool removeWhiteListIp(String byIp)
        {
            if (getIsWhitelistIp(byIp))
            {
                SQLiteConnection sqlite_conn = Database.CreateConnection();
                string sqlStatement;
                sqlStatement = "DELETE FROM WhiteList WHERE IP = " + "\"" + byIp.ToLower() + "\" COLLATE NOCASE";
                try
                {
                    var cmd = new SQLiteCommand(sqlStatement, sqlite_conn);
                    cmd.ExecuteNonQuery();
                }
                finally
                {
                    sqlite_conn.Close();
                }
                return true;
            }
            return false;
        }

        public static BlackList getBlackList(String byUrl)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            String ThreatName;
            String url;
            String ip;
            String date;
            BlackList b;
            string stm;
            SQLiteDataReader rdr;
            stm = "SELECT * FROM BlackList WHERE URL='" + byUrl + "' LIMIT 1";
            var cmd = new SQLiteCommand(stm, sqlite_conn);
            rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                ThreatName = rdr.GetString(1);
                url = rdr.GetString(2);
                ip = rdr.GetString(3);
                date = rdr.GetString(4);
                rdr.Close();
                b = new BlackList(ThreatName, url, ip, date);
                return b;
            }
            else
            {
                b = new BlackList("not in database", "not in database", "not in database", "not in database");
                return b;
            }
        }

        public static WhiteList getWhiteList(String byUrl)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            String ThreatName;
            String url;
            String ip;
            String date;
            WhiteList w;
            string stm;
            SQLiteDataReader rdr;
            stm = "SELECT * FROM WhiteList WHERE URL='" + byUrl + "' LIMIT 1";
            var cmd = new SQLiteCommand(stm, sqlite_conn);
            rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                ThreatName = rdr.GetString(1);
                url = rdr.GetString(2);
                ip = rdr.GetString(3);
                date = rdr.GetString(4);
                rdr.Close();
                w = new WhiteList(ThreatName, url, ip, date);
                return w;
            }
            else
            {
                w = new WhiteList("not in database", "not in database", "not in database", "not in database");
                return w;
            }
        }

        public static Warning getWarning(String byUrl)
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            String ThreatName;
            String url;
            String ip;
            String date;
            String details;
            Warning w;
            string stm;
            SQLiteDataReader rdr;
            stm = "SELECT * FROM Warnings WHERE URL='" + byUrl + "' LIMIT 1";
            var cmd = new SQLiteCommand(stm, sqlite_conn);
            rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                ThreatName = rdr.GetString(1);
                url = rdr.GetString(2);
                ip = rdr.GetString(3);
                date = rdr.GetString(4);
                details = rdr.GetString(5);
                rdr.Close();
                w = new Warning(ThreatName, url, ip, date, details);
                return w;
            }
            else
            {
                w = new Warning("not in database", "not in database", "not in database", "not in database", "not in database");
                return w;
            }
        }

        public static Threats getThreats()
        {
            SQLiteConnection sqlite_conn = Database.CreateConnection();
            String ThreatName;
            String url;
            String ip;
            String date;
            String details;
            Threats t;
            string stm;
            SQLiteDataReader rdr;
            stm = "SELECT * FROM Threats LIMIT 1";
            var cmd = new SQLiteCommand(stm, sqlite_conn);
            rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                ThreatName = rdr.GetString(1);
                url = rdr.GetString(2);
                ip = rdr.GetString(3);
                date = rdr.GetString(4);
                details = rdr.GetString(5);
                rdr.Close();
                t = new Threats(ThreatName, url, ip, date, details);
                return t;
            }
            else
            {
                t = new Threats("not in database", "not in database", "not in database", "not in database", "not in database");
                return t;
            }
        }
    }
}
