﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XrayWall
{
    class BlackList
    {
        private String ThreatName;
        private String url;
        private String ip;
        private String Date;

        public BlackList(String ThreatName, String url, String ip, String Date)
        {
            this.ThreatName = ThreatName;
            this.url = url;
            this.ip = ip;
            this.Date = Date;
        }
        public String getThreatName()
        {
            return this.ThreatName;
        }
        public String getUrl()
        {
            return this.url;
        }
        public String getIp()
        {
            return this.ip;
        }
        public String getDate()
        {
            return this.Date;
        }
    }
}
