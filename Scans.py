from datetime import datetime
import socket
from ServerDatabase import connectDatabase
from ServerDatabase import getData
from ServerDatabase import getScripts
import sqlite3
ScanResults = [] #the results

db = connectDatabase()

def https_scans(flow):
    #get the data from the flow and check if there is any possible attack on https like ddos or something else.
    ScanResults = ["0", "Protocol: https (port 443), Threat: DDOS."]
    return ScanResults

def responseScan(flow):
    if "google" not in str(flow.request.host):
        response = flow.response
        if "href" in str(response.content):
            hrefList = str(response.content).split("href")
            for line in hrefList[1:]:
                if "https" in str(line.split('"')[1]) and len(str(line.split('"')[1])) > 7:
                    #check if one of those links direct to another site so we can warn the user about redirects.
                    pass
                elif "http:" in str(line.split('"')[1]) and len(str(line.split('"')[1])) > 7:
                    #means there are pics / links that directing to unsafe (http) sites, should just warn the user.
                    ff= open("C:/Users/user/mitmp/successWarning.txt","a")
                    ff.write("good1\n")
                    ff.close()
                    detailsStr = "Link that redirect to unsafe site: " + str(line.split('"')[1])
                    detailsStr = detailsStr.replace("'", '"')
                    #get curr time as string:
                    now = datetime.now()
                    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
                    #get site ip as string:
                    #if theres problem then its probably cus the url, just for it in the response.
                    db.execute("INSERT INTO Warnings(ThreatName, URL, IP, DATE, Details) VALUES('Dangerous link', '" + str(flow.request.url).split("//")[1].split("/")[0] + "', '127.0.0.1', '" + dt_string + "', '" + detailsStr + "');")
        if "href" in str(flow.response.data):
            hrefList = str(flow.response.data).split("href")
            for line in hrefList[1:]:
                """
                if "https" in str(line.split('"')[1]) and len(str(line.split('"')[1])) > 7:
                    #check if one of those links direct to another site so we can warn the user about redirects.
                    pass
                """
                if "http:" in str(line.split('"')[1]) and len(str(line.split('"')[1])) > 7:
                    #means there are pics / links that directing to unsafe (http) sites, should just warn the user.
                    detailsStr = "Link that redirect to unsafe site: " + str(line.split('"')[1])
                    detailsStr = detailsStr.replace("'", '"')
                    #get curr time as string:
                    now = datetime.now()
                    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
                    #get site ip as string:
                    ip = socket.gethostbyname(str(request.host))
                    #if theres problem then its probably cus the url, just for it in the response.
                    db.execute("INSERT INTO Warnings(ThreatName, URL, IP, DATE, Details) VALUES('Dangerous link', '" + str(flow.request.url).split("//")[1].split("/")[0] + "', '" + ip + "', '" + dt_string + "', '" + detailsStr + "');")
                    db.commit()
        ScanResults = ["0", "Protocol: http (port 80), Threat: None"]
        return ScanResults

def http_scans(flow):
    #get the data from the flow and check if there is any possible http attack like xss, css or any script attack.
    if "script" in str(flow.request):
        ThreatStr = ""
        if '%' in str(flow.request):
            #convert all the hex to ascii so we can show it nicely to the user:
            SplitStr = str(flow.request).split("%")
            for line in SplitStr[1:]:
                line = bytearray.fromhex(line[:2]).decode() + line[2:]
                ThreatStr += line
        else: #case there is no hex and its already ascii
            ThreatStr = '<'  + str(flow.request).split('<')[1]
        ScriptsToScan = getScripts()
        if len(ScriptsToScan) == 0:
            TT = "Protocol: http (port 80), Threat: script in request (url), the threat: " + ThreatStr
            TT = TT.replace("'", '"')
            ScanResults = ["1", TT, "script in url"]
        else:
            for item in ScriptsToScan:
                if str(item["Script"]) in ThreatStr:
                    TT = str(item["Details"]) + ", the threat: " + ThreatStr
                    TT = TT.replace("'", '"')
                    ScanResults = ["1", TT, "script in url"]
    else:
        ScanResults = ["0", "Protocol: http (port 80), Threat: None"]
    return ScanResults

def Other_scans(flow):
    #check if there is any attack in other protocols, scan them and return results.
    ScanResults = ["0", "Protocol: unkown, Threat: unknown."]
    return ScanResults

def scansManager(flow, scanType):
    if(scanType == 0): #request
        request = flow.request
        if request.scheme == "https":
            return https_scans(flow)
        elif ('http' in str(request.url) and 'https' not in str(request.url)) or 'http.' in str(request.url):
            return http_scans(flow)
        else:
            return Other_scans(flow) #another protocol, pass for now.
    """
    else:#response
        ScanResults = ["0", "Protocol: http (port 80), Threat: None"]
        return ScanResults
    """
