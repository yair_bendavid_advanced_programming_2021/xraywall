from datetime import datetime
import netifaces
from time import sleep
import scapy.all as scapy
from threading import Thread
from ServerDatabase import connectDatabase
import sqlite3

gateways = netifaces.gateways()
default_gateway = gateways['default'][netifaces.AF_INET][0] #the router ip.

netifaces.gateways()
iface = netifaces.gateways()['default'][netifaces.AF_INET][1]
IP = [str(netifaces.ifaddresses(iface)[netifaces.AF_INET][0]['addr']) + '/24'] #your ip.

MacAdresses = [] #for arp spoofing scan.
ipAdresses = [] #for arp spoofing scan.
ThreatIp = "" #for arp spoofing scan.
test = [datetime.now()]
db = connectDatabase()

ArpTime = datetime.now() #for arp spoofing scan.

#check for arp spoofing attack every 5 minutes.
def CheckArpSpoofingOnTimer():
    while(True):
        #check if user has cd
        now = datetime.now()
        diff = now - test[0]
        ff= open("C:/Users/user/mitmp/timer.txt","a")
        ff.write(str(diff.total_seconds()) + "\n")
        ff.close()
        if diff.total_seconds() >= 300: #5 minutes passed so we can scan arp again.
            test[0] = datetime.now()
            arp_r = scapy.ARP(pdst=IP[0])
            br = scapy.Ether(dst='ff:ff:ff:ff:ff:ff')
            request = br/arp_r
            answered, unanswered = scapy.srp(request, timeout=1)
            for i in answered:
                ip, mac = i[1].psrc, i[1].hwsrc
                if str(mac) in MacAdresses: #found ARP attack!
                    #find the index:
                    index = MacAdresses.index(str(mac))
                    #now get the ip of that threat!
                    secondIp = ipAdresses[index]
                    #now check if one of them is our router!
                    if str(ip) == default_gateway or secondIp == default_gateway:
                        #we should hurry and find the fake so we can block any contact with him!
                        if str(ip) == default_gateway:
                            ThreatIp = secondIp
                        else:
                            ThreatIp = str(ip)
                        db.execute("INSERT INTO History(ThreatName, URL, IP, DATE, Details) VALUES('ARP Spoofing', '" + str(request.url).split("//")[1].split("/")[0] + "', '127.0.0.1', '23/11/2022', 'Protocol: ARP, found an Arp Spoofing attack by someone in your wifi (his ip is: " + ThreatIp + ") be aware!');")
                        db.commit()
                MacAdresses.append(str(mac))
                ipAdresses.append(str(ip))
            MacAdresses.clear()
            ipAdresses.clear()
        sleep(1)
