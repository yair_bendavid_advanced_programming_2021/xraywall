import sqlite3

def connectDatabase():
    #will choose the path to be the project path.
    db = sqlite3.connect("C:/Users/user/mitmp/database.db", check_same_thread=False)

    #create the BlackList table.
    db.execute('''CREATE TABLE IF NOT EXISTS BlackList
             (ID integer primary key autoincrement,
             ThreatName           TEXT    NOT NULL,
             URL            TEXT     NOT NULL,
             IP            TEXT     NOT NULL,
             DATE         TEXT     NOT NULL);''')

    #create the WhiteList table.
    db.execute('''CREATE TABLE IF NOT EXISTS WhiteList
             (ID integer primary key autoincrement,
             ThreatName           TEXT    NOT NULL,
             URL            TEXT     NOT NULL,
             IP            TEXT     NOT NULL,
             DATE         TEXT     NOT NULL);''')

    #create the History table.
    db.execute('''CREATE TABLE IF NOT EXISTS History
             (ID integer primary key autoincrement,
             ThreatName           TEXT    NOT NULL,
             URL            TEXT     NOT NULL,
             IP            TEXT     NOT NULL,
             DATE         TEXT     NOT NULL,
             Details        TEXT     NOT NULL);''')

    #create the Warning table.
    db.execute('''CREATE TABLE IF NOT EXISTS Warnings
             (ID integer primary key autoincrement,
             ThreatName           TEXT    NOT NULL,
             URL            TEXT     NOT NULL,
             IP            TEXT     NOT NULL,
             DATE         TEXT     NOT NULL,
             Details        TEXT     NOT NULL);''')
    #create the Threats table.
    db.execute('''CREATE TABLE IF NOT EXISTS Threats
             (ID integer primary key autoincrement,
             ThreatName           TEXT    NOT NULL,
             URL            TEXT     NOT NULL,
             IP            TEXT     NOT NULL,
             DATE         TEXT     NOT NULL,
             Details        TEXT     NOT NULL);''')
    #create URLS table for the Warnings table.
    db.execute('''CREATE TABLE IF NOT EXISTS URLS
             (ID integer primary key autoincrement,
             URL            TEXT     NOT NULL,
             SCANNED            TEXT     NOT NULL);''')
    #create Scripts table for the Protection level.
    db.execute('''CREATE TABLE IF NOT EXISTS Scripts
             (Script            TEXT     NOT NULL,
             Details            TEXT     NOT NULL);''')
    #create DatabaseSettings table for the Protection level.
    db.execute('''CREATE TABLE IF NOT EXISTS DatabaseSettings
             (ProtectionLevel            TEXT     NOT NULL);''')

    """
    #example to insert into BlackList database:
    db.execute("INSERT INTO BlackList(ThreatName, URL, IP, DATE) VALUES('DDOS', 'www.facebook.com', '127.0.0.1', '23/11/2022');");
    
    #example to insert into WhiteList database:
    db.execute("INSERT INTO WhiteList(URL, IP, DATE) VALUES( 'www.facebook.com', '127.0.0.1', '23/11/2022');");
    
    #example to insert into History database:
    db.execute("INSERT INTO History(ThreatName, URL, IP, DATE, Details) VALUES('DDOS', 'www.facebook.com', '127.0.0.1', '23/11/2022', 'Found an ddos attempt from this ip');")
    
    #commit the records into the database.
    db.commit()
    #get all the records from a table in the database (History for example):
    Records = []
    results = db.execute("SELECT ID, ThreatName, URL, IP, DATE, Details from History")
    for row in results:
       Records.append({'id': row[0], 'ThreatName': row[1], 'URL': row[2], 'IP': row[3], 'DATE': row[4], 'Details': row[5]})

    #print the list of dicts of the records to show the results as an example.
    print(Records)
    """
    #some rows for tests.
    db.commit()
    return db

def getData():
    db = sqlite3.connect("C:/Users/user/mitmp/database.db", check_same_thread=False)
    Records = []
    results = db.execute("SELECT ID, ThreatName, URL, IP, DATE from BlackList")
    for row in results:
        Records.append({'id': row[0], 'ThreatName': row[1], 'URL': row[2], 'IP': row[3], 'DATE': row[4]})
    return Records

def getDataWhiteList():
    db = sqlite3.connect("C:/Users/user/mitmp/database.db", check_same_thread=False)
    Records = []
    results = db.execute("SELECT ID, ThreatName, URL, IP, DATE from WhiteList")
    for row in results:
        Records.append({'id': row[0], 'ThreatName': row[1], 'URL': row[2], 'IP': row[3], 'DATE': row[4]})
    return Records

def getDataURLS():
    db = sqlite3.connect("C:/Users/user/mitmp/database.db", check_same_thread=False)
    Records = []
    results = db.execute("SELECT ID, URL, SCANNED from URLS")
    for row in results:
        Records.append({'id': row[0], 'URL': row[1], 'SCANNED': row[2]})
    return Records

def getScripts():
    db = sqlite3.connect("C:/Users/user/mitmp/database.db", check_same_thread=False)
    Records = []
    results = db.execute("SELECT Script, Details from Scripts")
    for row in results:
        Records.append({'Script': row[0], 'Details': row[1]})
    return Records
