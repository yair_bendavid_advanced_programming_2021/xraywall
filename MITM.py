#!/usr/bin/python 
from mitmproxy import ctx, http
from datetime import datetime
from ArpScan import CheckArpSpoofingOnTimer
from warningScan import scan_Warnings
from Scans import scansManager
from ServerDatabase import connectDatabase
from ServerDatabase import getData
from ServerDatabase import getDataWhiteList
from ServerDatabase import getDataURLS
import sqlite3
import socket
import time
from threading import Thread
ScanResults = [] #the results

#will choose the path to be the project path.
db = connectDatabase()
Blocked_ips = []
Blocked_ips2 = []
urls = []
Trusted_ips = []

#start the thread for the arp spoofing scan.
thread = Thread(target=CheckArpSpoofingOnTimer)
thread.start()
#start the thread for the warnings scan.
WarningsT = Thread(target=scan_Warnings)
WarningsT.start()

def request(flow):
    # Access the current request object.
    request = flow.request
    Records = getData()
    RecordsWhite = getDataWhiteList()
    RecordUrls = getDataURLS()
    Blocked_ips2.clear()
    Blocked_ips.clear()
    Trusted_ips.clear()
    urls.clear()

    #get all the ips.
    for item in Records:
        Blocked_ips.append(item['URL'])
        Blocked_ips2.append(item['IP'])

    for item in RecordsWhite:
        Trusted_ips.append(item['URL'])

    for item in RecordUrls:
        urls.append(item['URL'])

    if str(request.url).split("//")[1].split("/")[0] not in str(" ".join(Trusted_ips)):
        if str(request.url).split("//")[1].split("/")[0] not in str(" ".join(Blocked_ips)):
            if str(socket.gethostbyname(str(request.host))) not in str(" ".join(Blocked_ips2)):
                if str(request.host) not in str(" ".join(urls)):
                    url = str(request.url).split("//")[0] + "//" + str(request.url).split("//")[1].split("/")[0] + "/"
                    if url not in str(" ".join(urls)):
                        db.execute("INSERT INTO URLS(URL, SCANNED) VALUES('" + url + "', '0');");
                        db.commit()
                ScanResults = scansManager(flow, 0) #its not one of the bannable ips so we can send to scans
                flag = ScanResults[0]
        else:
            request.url = "https://www.google.com"
        # we can change the request if we want (in case we dont want to get into this site for example).
        if flag == "1": #here the condition to when redirect, for example if the ip/site in block list.
            Blocked_ips.append(str(request.url).split("//")[1].split("/")[0])
            #update the new blocked to the db:
            now = datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            ip = socket.gethostbyname(str(request.host))
            db.execute("INSERT INTO BlackList(ThreatName, URL, IP, DATE) VALUES('" + ScanResults[2] + "', '" + str(request.url).split("//")[1].split("/")[0] + "', '" + ip + "', '" + dt_string + "');")
            db.execute("INSERT INTO History(ThreatName, URL, IP, DATE, Details) VALUES('" + ScanResults[2] + "', '" + str(request.url).split("//")[1].split("/")[0] + "', '" + ip + "', '" + dt_string + "', '" + ScanResults[1] + "');")
            db.execute("INSERT INTO Threats(ThreatName, URL, IP, DATE, Details) VALUES('" + ScanResults[2] + "', '" + str(request.url).split("//")[1].split("/")[0] + "', '" + ip + "', '" + dt_string + "', '" + ScanResults[1] + "');")
            db.commit()
            request.url = "https://www.google.com"
            #also change more params in the future to make it look better and even also for our own site.
            #also modify here the respone so we can see it was redirected.
def response(flow):
    # Access the current response object.
    #response = flow.response
    #ScanResults = scansManager(flow, 1)

    """
    if ScanResults[0] == "1":
        pass #found threat, so do not get that response, redirect to google.com and add the threat.
    """

    """
    #we can modify the info we get from that site (for example if we detected an attack we can redirect to google.
    flow.response = http.HTTPResponse.make(
            302,  # redirect code
            b"",  # content (empty for now)
            {"Content-Type": "text/html"}  # headers.
        )
    f.write(str(flow.response) + "\nnew line\n")
    f.close()
    #log the changed (wont be needed for that in the future when we have everything in the GUI).
    ctx.log.info(f"Modified response: {response}")
    """



"""
    def DHCP(flow):
        if "discover" in flow.messages[0].content:
                # Create a new DHCP offer message
            msg = DHCP_Offer(flow.client_conn.address, flow.server_conn.address)
                # Send the offer message
            flow.client_conn.send(msg.to_bytes())
    
        elif "request" in flow.messages[0].content:
            # Create a new DHCP ack message
            msg = DHCP_Ack(flow.client_conn.address, flow.server_conn.address, flow.messages[0].options["requested_addr"],
                           flow.messages[0].chaddr)
            # Send the ack message
            flow.client_conn.send(msg.to_bytes())
    
    
    
    
    def DHCP_Offer(src_addr, dest_addr):
    
        # Create the message object
        msg = DHCP()
        # Set the message type to "offer"
        msg.op = 2
        # Set the transaction ID
        msg.xid = random.randint(0, 2  32 - 1)
        # Set the offered IP address
        msg.yiaddr = offer_ip
        # Set the server address
        msg.siaddr = dest_addr
        # Add the necessary options
        msg.options.update({
            "message-type": 2,  # offer
            "server_id": dest_addr,
            "leasetime": 86400,  # 24 hours
            "renewaltime": 43200,  # 12 hours
            "rebindingtime": 75600  # 21 hours
        })
        return msg
    
    
    def DHCP_Ack(src_addr, dest_addr, request_ip, request_mac):
        # Create the message object
        msg = DHCP()
        # Set the message type to "ack"
        msg.op = 5
        # Set the transaction ID
        msg.xid = random.randint(0, 232 - 1)
        # Set the client address
        msg.yiaddr = request_ip
        # Set the server address
        msg.siaddr = dest_addr
        # Set the client MAC address
        msg.chaddr = request_mac
        # Add the necessary options
        msg.options.update({
            "message-type": 5,  # ack
            "server_id": dest_addr,
            "leasetime": 86400,  # 24 hours
            "renewaltime": 43200,  # 12 hours
            "rebindingtime": 75600  # 21 hours
        })
        return msg
"""

"""
 #get the info from the database.
    Records = []
    Blocked_ips = []
    blocked_ips = db.execute("SELECT ID, ThreatName, URL, IP, DATE from BlackList")
    for row in results:
        Records.append({'id': row[0], 'ThreatName': row[1], 'URL': row[2], 'IP': row[3], 'DATE': row[4]})

    #close the database when we dont need it anymore (after we close the server for example).
    db.close()

    # we can change the request if we want (in case we dont want to get into this site for example).
    #get all the ips.
    for item in Records:
        Blocked_ips.append(item['URL'])
    f.write(str("".join(Blocked_ips)) + "\n")
    f.write("aaaa\n")
    f.close()
"""




"""
from mitmproxy import ctx

def request(flow):
    # Access the current request object.
    request = flow.request

    # Modify the request as needed.
    request.url = "www.google.com"

    # Log the changes that were made.
    ctx.log.info(f"Modified request: {request}")

def response(flow):

    # Access the current response object.
    response = flow.response

    # Modify the response as needed.
    #response.text = "Hello, world!"

    # Log the changes that were made.
    ctx.log.info(f"Modified response: {response}")
 """
