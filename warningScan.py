import urllib
from urllib.request import Request, urlopen
from datetime import datetime
import socket
import sqlite3
from ServerDatabase import connectDatabase
from ServerDatabase import getDataURLS

db = connectDatabase()

def get_domain(url):
    if "//" in url:
        completed_domain = url.split("//")[1]
        if "/" in completed_domain:
            return completed_domain[:-1]
        else:
            return completed_domain
    return url

def scan_Warnings():
    while True:
        try:
            URL = ""
            RecordUrls = getDataURLS()
            for item in RecordUrls:
                if item["SCANNED"] == "0":
                    URL = item["URL"]
            if(len(URL) > 0):
                db.execute("UPDATE URLS SET SCANNED='1' WHERE URL='" + URL + "'");
                db.commit()
                unsafeSites = ""
                anotherSites = ""
                req = Request(url=URL, headers={'User-Agent': 'Mozilla/5.0'})
                web_byte = urlopen(req).read()
                webpage = str(web_byte.decode('utf-8'))
                if "href" in webpage:
                    hrefList = webpage.split("href")
                    for line in hrefList[1:]:
                        if '"' in line:
                            domainLink = str(line.split('"')[1])
                            if "https" in domainLink and len(domainLink) > 7:
                                if get_domain(URL) not in domainLink:
                                    anotherSites += domainLink + " "
                                    anotherSites = anotherSites.replace("'", '"')
                                pass
                            elif "http:" in domainLink and len(domainLink) > 7:
                                if get_domain(URL) not in domainLink:
                                    unsafeSites += domainLink + " "
                                    unsafeSites = unsafeSites.replace("'", '"')
                    #warning details:
                    detailsStr = ""
                    if len(anotherSites) > 0:
                        detailsStr = "Links to another sites: " + anotherSites[:-1] + "\n"
                    if len(unsafeSites) > 0:
                        detailsStr += "unsafe sites: " + unsafeSites[:-1]
                    #get curr time as string:
                    now = datetime.now()
                    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
                    ip = socket.gethostbyname(get_domain(URL))
                    #if theres problem then its probably cus the url, just for it in the response.
                    db.execute("INSERT INTO Warnings(ThreatName, URL, IP, DATE, Details) VALUES('Dangerous link', '" + URL + "', '" + ip + "', '" + dt_string + "', '" + detailsStr + "');")
                    db.commit()
        except urllib.error.HTTPError as e:
            pass
        except urllib.error.URLError as e: #means the user entered invalid url
            pass
        except (RuntimeError, TypeError, NameError, ValueError):
            pass


#if i want the user to be able to add domains to scan in the gui then i should add to the URLS table IsUser which tell me if the user triggered it.
#and also return error if there is / the result immediatly to his gui window (add to Warning table isUser too).
"""
error = scan_Warnings("https://theboxerwebtoon.com/")
if error:
    if len(error) > 0:
        print(error)
"""
